package biblioteca.util;

import biblioteca.model.Carte;

import java.util.Calendar;

public class Validator {
	
	public static boolean isStringOK(String s) throws Exception{
		boolean flag = s.matches("[a-zA-Z]+( )*[a-zA-Z]*");
		if(!flag)
			throw new Exception("String invalid");
		return true;
	}
	
	public static void validateCarte(Carte c)throws Exception{
		if(c.getCuvinteCheie()==null || c.getCuvinteCheie().size() == 0){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		else if (c.getCuvinteCheie().size() > 20){
			throw new Exception("Lista cuvinte cheie depaseste capacitatea maxima!");
		}
		else if(c.getReferenti()==null || c.getCuvinteCheie().size() == 0){
			throw new Exception("Lista autori vida!");
		}
		else if (c.getReferenti().size() > 20){
			throw new Exception("Lista autori depaseste capacitatea maxima!");
		}
		else if(!isOKString(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		else if (c.getTitlu().length() > 100)
			throw  new Exception("Titlul poate avea maxim 100 de caractere lungime!");
		else if(!Validator.isNumber(c.getAnAparitie()))
			throw new Exception("An aparitie trebuie sa fie un numar!");

		int an = Integer.parseInt(c.getAnAparitie());
		if (an < 1000)
			throw new Exception("Nu exista inregistrari cu aparitii mai vechi de anul 1000!");
		else if (an > Calendar.getInstance().get(Calendar.YEAR))
			throw new Exception("Anul aparitiei nu poate depasi anul curent!");

		for(String s:c.getReferenti()){
			if(!isOKString(s))
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()){
			if(!isOKString(s))
				throw new Exception("Cuvant cheie invalid!");
		}
	}
	
	public static boolean isNumber(String s){
		if (s.isEmpty())
			return false;
		else {
			try {
				Integer.parseInt(s);
			}
			catch (NumberFormatException e){
				return false;
			}
			return true;
		}
	}
	
	private static boolean isOKString(String s){
		String []t = s.split(" ");
		if(t.length==2){
			boolean ok1 = t[0].matches("[a-zA-Z0-9]+");
			boolean ok2 = t[1].matches("[a-zA-Z0-9]+");
			if(ok1 == ok2)
				return ok1;

		}
		return s.matches("[a-zA-Z0-9]+");
	}
	
}

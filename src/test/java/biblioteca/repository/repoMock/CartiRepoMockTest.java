package biblioteca.repository.repoMock;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

//testare WBT
public class CartiRepoMockTest {

    List<String> referenti = new ArrayList<String>();
    String editura;
    String titlu;
    String anAparitie;
    List<String> cuvCheie = new ArrayList<String>();
    private CartiRepoInterface cartiRepo;


    @Before
    public void setUp() throws Exception {
        cartiRepo = new CartiRepoMock();

        cartiRepo.getCarti().clear();

    }

    @Test
    public void cautaCarteEmpty() {
        List<Carte> cartes = cartiRepo.cautaCarte("a");
        assertEquals(cartes.size(), 0);
    }

    @Test
    public void cautaCarteNoReferentFound() {
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        referenti.add("Referent");
        cuvCheie.add("keyword");
        Carte c = new Carte(titlu, referenti, anAparitie, editura, cuvCheie);
        cartiRepo.adaugaCarte(c);
        List<Carte> cartes = cartiRepo.cautaCarte("a");
        assertEquals(cartes.size(), 0);
    }

    @Test
    public void cautaCarteReferent() {
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        referenti.add("Referent");
        cuvCheie.add("keyword");
        Carte c = new Carte(titlu, referenti, anAparitie, editura, cuvCheie);
        cartiRepo.adaugaCarte(c);
        List<Carte> cartes = cartiRepo.cautaCarte("ref");
        assertEquals(cartes.size(), 1);
    }

    @Test
    public void cautaCarteNoReferent() {
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        cuvCheie.add("keyword");
        Carte c = new Carte(titlu, referenti, anAparitie, editura, cuvCheie);
        cartiRepo.adaugaCarte(c);
        List<Carte> cartes = cartiRepo.cautaCarte("a");
        assertEquals(cartes.size(), 0);
    }

    @Test
    public void cautaCarteNull() {
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        referenti.add("");
        cuvCheie.add("keyword");
        Carte c = new Carte(titlu, referenti, anAparitie, editura, cuvCheie);
        cartiRepo.adaugaCarte(c);
        try{
            List<Carte> cartes = cartiRepo.cautaCarte(null);
        }
        catch (NullPointerException exc){

        }
    }
}
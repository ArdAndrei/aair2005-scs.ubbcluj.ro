package biblioteca;

import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

//WBT
public class AfisareCartiTest {
    List<String> referenti = new ArrayList<String>();
    String editura;
    String titlu;
    String anAparitie;
    List<String> cuvCheie = new ArrayList<String>();
    private CartiRepoInterface cartiRepo;

    @Before
    public void setUp() throws Exception {
        cartiRepo = new CartiRepoMock();
    }


    // WBT
    // afisare carti ordonate din an null
    // returneaza o lista vida
    @Test
    public void getCartiOrdonateDinAnNull() {
        List<Carte> cartes = cartiRepo.getCartiOrdonateDinAnul(null);
        assertEquals(cartes.size(), 0);
    }

    // WBT
    // afisare carti ordonate din an 1973
    // returneaza o lista continand cartile care au aparut in anul 1973
    // ordonate alfabetic dupa titlu si autor
    @Test
    public void getCartiOrdonateDinAnExistent() {
        String anAparitie = "1973";
        List<Carte> cartes = cartiRepo.getCartiOrdonateDinAnul(anAparitie);
        assertNotEquals(cartes.size(), 0);
        for (Carte c: cartes
                ) {
            assertEquals(c.getAnAparitie(), anAparitie);
        }
    }

}

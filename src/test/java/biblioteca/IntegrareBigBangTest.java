package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import biblioteca.view.Consola;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class IntegrareBigBangTest {
    List<String> referenti = new ArrayList<String>();
    String editura;
    String titlu;
    String anAparitie;
    List<String> cuvCheie = new ArrayList<String>();


    CartiRepoInterface cartiRepo;
    BibliotecaCtrl ctrl;

    @Before
    public void setUp(){
        cartiRepo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(cartiRepo);
    }

    @Test
    public void testAdauga(){
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        referenti.add("Referent");
        cuvCheie.add("keyword");

        // test case record added
        try {
            // already 6 records in repo
            assertEquals(ctrl.getCarti().size(), 6);
            // add new record
            ctrl.adaugaCarte(titlu, anAparitie, editura, referenti, cuvCheie);
            assertEquals(ctrl.getCarti().size(), 7);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Test
    public void testCauta(){
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        referenti.add("Referent");
        cuvCheie.add("keyword");
        Carte c = new Carte(titlu, referenti, anAparitie, editura, cuvCheie);
        cartiRepo.adaugaCarte(c);
        List<Carte> cartes = cartiRepo.cautaCarte("ref");
        assertEquals(cartes.size(), 1);
    }


    @Test
    public void testAfisareOrdonat(){
        String anAparitie = "1973";
        List<Carte> cartes = cartiRepo.getCartiOrdonateDinAnul(anAparitie);
        assertNotEquals(cartes.size(), 0);
        for (Carte c: cartes
             ) {
            assertEquals(c.getAnAparitie(), anAparitie);
        }
    }



    @Test
    public void testIntegrare() throws Exception {
        // testare dimensiunea curenta a cartilor din anul 1973
        String anAparitie = "1973";
        List<Carte> cartes = cartiRepo.getCartiOrdonateDinAnul(anAparitie);
        int size = cartes.size();

        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1973";
        referenti.add("Referent");
        cuvCheie.add("keyword");

        // test case record added
        try {
            // already 6 records in repo
            assertEquals(ctrl.getCarti().size(), 6);
            // add new record
            ctrl.adaugaCarte(titlu, anAparitie, editura, referenti, cuvCheie);
            assertEquals(ctrl.getCarti().size(), 7);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Carte> cartiCautate = cartiRepo.cautaCarte("ref");
        assertEquals(cartiCautate.size(), 1);

        cartes = cartiRepo.getCartiOrdonateDinAnul(anAparitie);
        assertEquals(cartes.size(), size + 1);
        for (Carte c: cartes
                ) {
            assertEquals(c.getAnAparitie(), anAparitie);
        }
    }
}

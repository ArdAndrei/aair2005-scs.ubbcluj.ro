package biblioteca;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class IntegrareTopDownTest {
    List<String> referenti = new ArrayList<String>();
    String editura;
    String titlu;
    String anAparitie;
    List<String> cuvCheie = new ArrayList<String>();


    CartiRepoInterface cartiRepo;
    BibliotecaCtrl ctrl;

    @Before
    public void setUp(){
        cartiRepo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(cartiRepo);
    }

    @Test
    public void testAdauga(){
        titlu = "Titlu";
        editura = "editura";
        referenti.add("Referent");
        cuvCheie.add("keyword");
        anAparitie = "1000";
        // test case an aparitie 1000
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            //succes
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void testIntegrareCauta(){
        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1923";
        referenti.add("Referent");
        cuvCheie.add("keyword");

        // test case record added
        try {
            // already 6 records in repo
            assertEquals(ctrl.getCarti().size(), 6);
            // add new record
            ctrl.adaugaCarte(titlu, anAparitie, editura, referenti, cuvCheie);
            assertEquals(ctrl.getCarti().size(), 7);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Carte> cartiCautate = cartiRepo.cautaCarte("ref");
        assertEquals(cartiCautate.size(), 1);
    }

    @Test
    public void testIntegrareAfisareOrdonat(){
        String anAparitie = "1973";
        List<Carte> cartes = cartiRepo.getCartiOrdonateDinAnul(anAparitie);
        int size = cartes.size();

        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1973";
        referenti.add("Referent");
        cuvCheie.add("keyword");

        // test case record added
        try {
            // already 6 records in repo
            assertEquals(ctrl.getCarti().size(), 6);
            // add new record
            ctrl.adaugaCarte(titlu, anAparitie, editura, referenti, cuvCheie);
            assertEquals(ctrl.getCarti().size(), 7);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Carte> cartiCautate = cartiRepo.cautaCarte("ref");
        assertEquals(cartiCautate.size(), 1);

        cartes = cartiRepo.getCartiOrdonateDinAnul(anAparitie);
        assertEquals(cartes.size(), size + 1);
        for (Carte c: cartes
                ) {
            assertEquals(c.getAnAparitie(), anAparitie);
        }
    }

}

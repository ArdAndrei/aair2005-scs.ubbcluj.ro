package biblioteca.control;

import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.repository.repoMock.CartiRepoMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static org.junit.Assert.*;

// testare BBT
public class BibliotecaCtrlTest {
    BibliotecaCtrl ctrl;
    String titlu, editura, anAparitie;
    List<String> referenti = new ArrayList<String>();
    List<String> cuvCheie = new ArrayList<String>();


    @Before
    public void setUp() throws Exception {
        CartiRepoInterface repo = new CartiRepoMock();
        ctrl = new BibliotecaCtrl(repo);

        titlu = "Titlu";
        editura = "editura";
        anAparitie = "1920";
        referenti.add("Referent");
        cuvCheie.add("keyword");
    }

    @Test
    public void adaugaCarteRecordAdded() {
        // test case record added
        try {
            // already 6 records in repo
            assertEquals(ctrl.getCarti().size(), 6);
            // add new record
            ctrl.adaugaCarte(titlu, anAparitie, editura, referenti, cuvCheie);
            assertEquals(ctrl.getCarti().size(), 7);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void adaugaCarteCuvCheieVida() {
        cuvCheie.clear();
        // test case lista cuvinte cheie vida
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            fail();
        } catch (Exception e) {
            assertEquals("Lista cuvinte cheie vida!", e.getMessage());
        }
    }

    @Test
    public void adaugaCarteCuvCheieCapacitateDepasita() {
        for (int i = 0; i < 20; i++){
            cuvCheie.add("keyword" + i);
        }
        // test case lista cuvinte cheie depasita
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            fail();
        } catch (Exception e) {
            assertEquals("Lista cuvinte cheie depaseste capacitatea maxima!", e.getMessage());
        }
    }


    @Test
    public void adaugaCarteAnAparitieNuENumar() {

        anAparitie = "20siunu";
        // test case an aparitie nu e numar
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            fail();
        } catch (Exception e) {
            assertEquals("An aparitie trebuie sa fie un numar!", e.getMessage());
        }
    }


    @Test
    public void adaugaCarteAnAparitie1000() {
        anAparitie = "1000";
        // test case an aparitie 1000
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void adaugaCarteAnAparitie999() {
        anAparitie = "999";
        // test case an aparitie 1000
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            fail();
        } catch (Exception e) {
            assertEquals("Nu exista inregistrari cu aparitii mai vechi de anul 1000!", e.getMessage());
        }
    }

    @Test
    public void adaugaCarteAnAparitie1001() {
        anAparitie = "1001";
        // test case an aparitie 1000
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void adaugaCarteAnAparitieMaxOffByOne() {
        anAparitie = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) - 1);
        // test case an aparitie anul trecut
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void adaugaCarteAnAparitieMax() {
        anAparitie = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
        // test case an aparitie anul curent
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void adaugaCarteAnAparitieMaxPlusOne() {
        anAparitie = String.valueOf(Calendar.getInstance().get(Calendar.YEAR) + 1);
        // test case an aparitie anul viitor
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            fail();
        } catch (Exception e) {
            assertEquals("Anul aparitiei nu poate depasi anul curent!", e.getMessage());
        }
    }

    @Test
    public void adaugaCarteTitluLungime100() {
        StringBuilder builder = new StringBuilder();
        for (int i =0 ;i < 100; i++){
            builder.append("a");
        }
        titlu = builder.toString();
        // test case titlu de lungime 100
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
        } catch (Exception e) {
            fail();
        }
    }


    @Test
    public void adaugaCarteTitluLungime99() {
        StringBuilder builder = new StringBuilder();
        for (int i =0 ;i < 99; i++){
            builder.append("a");
        }
        titlu = builder.toString();
        // test case titlu de lungime 99
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);

        } catch (Exception e) {
            fail();
        }
    }


    @Test
    public void adaugaCarteTitluLungime101() {
        StringBuilder builder = new StringBuilder();
        for (int i =0 ;i < 101; i++){
            builder.append("a");
        }
        titlu = builder.toString();
        // test case titlu de lungime 101
        try {
            ctrl.adaugaCarte(titlu, anAparitie,editura,referenti,cuvCheie);
            fail();
        } catch (Exception e) {
            assertEquals("Titlul poate avea maxim 100 de caractere lungime!", e.getMessage());
        }
    }
}